---
title: 'Lab network analysis: Reformat expert testing facilities data long to wide'
output:
  html_document:
    df_print: paged
---


Goal: pivot expert testing labs from long format, with a row for each quarter, to wide format, with a column for each quarter and data type, and a single row for each facility. Drop the last columns with only calculations (rate) in them. Lab names should be standardized, but the quarters may be missing.

 
```{r libraries, include=FALSE}
library(readxl)
library(tidyverse)
library(tidylog)
library(here)
library(openxlsx)
```
 
```{r data-in}
xpert_raw_2019 <- read_excel(here("intake-data/06_ZMB_Xpert_testing_raw_abt.xlsx"), "Xpert_2019")
glimpse(xpert_raw_2019)

xpert_raw_2020 <- read_excel(here("intake-data/06_ZMB_Xpert_testing_raw_abt.xlsx"), "Xpert_2020")
glimpse(xpert_raw_2020)

```

```{r pivot-2019}
xpert_long_2019 <- 
  xpert_raw_2019 %>%  
  select(-contains("Rate")) %>% 
  pivot_longer( cols =  `Total Samples Tested`:`No Result`, names_to = "measure", values_to= "value" ) %>%
  group_by(Province, District, `Xpert Site`, Quarter) %>% 
  mutate(year = 2019, measure_order = row_number()) %>%
  mutate(measure = paste0( Quarter, " 2019" ,": ", measure)) %>% 
  arrange(Province, District, `Xpert Site`, Quarter, measure_order) %>% 
  distinct ()


xpert_wide_2019 <- xpert_long_2019  %>% 
  select(-measure_order) %>% 
  ungroup() %>% 
  select(-Quarter) %>%  
  pivot_wider(id_cols = Province:`Xpert Site`, names_from = measure, values_from = value)
 
stopifnot(nrow(xpert_wide_2019 [ duplicated(xpert_wide_2019$`Xpert Site`), ])==0) # check for errors; duplicates and missings
 stopifnot( all(xpert_raw_2019$`Xpert Site` %in%  xpert_wide_2019$`Xpert Site`  ) ) 

  xpert_wide_2019 

```


```{r pivot-2020}
xpert_raw_2020 <- 
  xpert_raw_2020 %>%  
  select(-contains("Rate")) %>% 
  mutate(District = case_when (`Xpert Site` == "Luano Mini Hospital/Masansa Rural Health Center" ~ "Luano",# standardize district
                               `Xpert Site`== "Peter Singogo" ~ "Ndola",
                               TRUE ~ District)) %>%
  group_by(Province, District, `Xpert Site`, Quarter) %>% 
  distinct() %>% # drop complete duplicates
  filter(across (`Total Samples Tested`:`No Result`, ~!is.na(.)))   # drop records where all are na

error_list <- xpert_raw_2020  %>% 
  filter((n()>1)) %>% ungroup() %>% 
  select(Province, District, `Xpert Site` )%>% unique()%>%                          
  mutate(flag_notes =   "multiple rows for a quarter in 2020" )   # Find duplicates here and create a flag column (only chama community district hospital)

xpert_long_2020 <- xpert_raw_2020 %>%
  pivot_longer( cols =  `Total Samples Tested`:`No Result`, names_to = "measure", values_to= "value" ) %>%
  group_by(Province, District, `Xpert Site`, Quarter) %>% 
  mutate(year = 2020, measure_order = row_number()) %>%
  mutate(measure = paste0( Quarter, " 2020" ,": ", measure)) %>%
  arrange(Province, District, `Xpert Site`, Quarter, measure_order) %>%
  distinct ()

xpert_wide_2020 <-  xpert_long_2020  %>%
  select(-measure_order) %>%
  ungroup() %>%
  select(-Quarter) %>%
  pivot_wider(id_cols = Province:`Xpert Site`, names_from = measure, values_from = value, values_fill= NA)
 
 stopifnot(nrow(xpert_wide_2020 [ duplicated(xpert_wide_2020$`Xpert Site`), ])==0) # check for errors; duplicates and missings; missings are all na for data rows, drop condition
```


```{r bind-pivot-years}
missing_2020 <- anti_join(xpert_wide_2019, xpert_wide_2020) %>% select(Province, District, `Xpert Site`) %>% mutate(flag_notes = "no 2020 data for site")
missing_2019 <- anti_join(xpert_wide_2020, xpert_wide_2019)  %>% select(Province, District, `Xpert Site`) %>% mutate(flag_notes = "no 2019 data for site")
error_list <- error_list %>% bind_rows(missing_2019, missing_2020)

xpert_wide_combined <- bind_rows(xpert_long_2019, xpert_long_2020) %>% 
  arrange( year,  Quarter, measure_order,Province, District, `Xpert Site`) %>% 
  select(-measure_order) %>%
  ungroup() %>%
  select(-Quarter)%>% 
  left_join(error_list)   %>% distinct() %>% 
  select(Province:`Xpert Site`, flag_notes, everything())%>%
  pivot_wider(id_cols = Province:flag_notes, names_from = measure, values_from = value, values_fill= NA) 
  
xpert_wide_combined %>% filter(!is.na(flag_notes))
xpert_wide_combined %>% filter(District == "Chama")
```


```{r pivot-export}
write.xlsx(xpert_wide_combined, here("output/02_cleaned/06_ZMB_Xpert_testing_wide_qtr-grp_2019-2020.xlsx"), sheetName = "Xpert_2019_2020")
```

```{r session-info}
print(sessionInfo())
```

